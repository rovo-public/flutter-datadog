package com.rovo.plugins.datadog;

import android.app.Application;

import androidx.annotation.NonNull;

import com.datadog.android.Datadog;
import com.datadog.android.DatadogConfig;
import com.datadog.android.log.Logger;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * DatadogPlugin
 */
public class FlutterDatadogPlugin implements MethodCallHandler {
    private final Application application;
    private Logger logger;

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_datadog");
        channel.setMethodCallHandler(new FlutterDatadogPlugin((Application) registrar.context()));
    }

    private FlutterDatadogPlugin(Application application) {
        this.application = application;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("initialize")) {
            final String token = call.argument("token");
            final String environment = call.argument("environment");

            DatadogConfig config = new DatadogConfig.Builder(token)
                    .setEnvironmentName(environment)
                    .setServiceName("WP Android")
                    .setCrashReportsEnabled(true)
                    .setLogsEnabled(true)
                    .build();
            Datadog.initialize(this.application.getApplicationContext(), config);

            this.logger = new Logger.Builder()
                    .setNetworkInfoEnabled(true)
                    .setDatadogLogsEnabled(true)
                    .setBundleWithTraceEnabled(true)
                    .setLoggerName("WP Android")
                    .build();

            result.success(true);
        } else if (call.method.equals("log")) {
            final String level = call.argument("level");
            final String event = call.argument("event");

            final String partyId = call.argument("partyId");
            final String profileId = call.argument("profileId");
            final String name = call.argument("name");
            final String sessionId = call.argument("sessionId");
            final String projectVersion = call.argument("projectVersion");
            final String txQuality = call.argument("txQuality");
            final String rxQuality = call.argument("rxQuality");
            final String localVideoStatsAgora = call.argument("localVideoStatsAgora");
            final String remoteVideoStatsAgora = call.argument("remoteVideoStatsAgora");
            final String localState = call.argument("localState");
            final String agoraError = call.argument("agoraError");
            final String message = call.argument("message");

            this.logger.addAttribute("partyId", partyId);
            this.logger.addAttribute("profileId", profileId);
            this.logger.addAttribute("name", name);
            this.logger.addAttribute("sessionId", sessionId);
            this.logger.addAttribute("projectVersion", projectVersion);
            this.logger.addAttribute("txQuality", txQuality);
            this.logger.addAttribute("rxQuality", rxQuality);
            this.logger.addAttribute("localVideoStatsAgora", localVideoStatsAgora);
            this.logger.addAttribute("remoteVideoStatsAgora", remoteVideoStatsAgora);
            this.logger.addAttribute("localState", localState);
            this.logger.addAttribute("agoraError", agoraError);
            this.logger.addAttribute("message", message);

            if (level.equals("WARN")) {
                this.logger.w(event);
            } else if (level.equals("ERROR")) {
                this.logger.e(event);
            } else {
                this.logger.i(event);
            }

            result.success(true);
        } else {
            result.notImplemented();
        }
    }
}
