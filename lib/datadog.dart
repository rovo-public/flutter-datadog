import 'dart:async';

import 'package:flutter/services.dart';

enum LogLevel { INFO, WARN, ERROR }

class Datadog {
  static const MethodChannel _channel = const MethodChannel('flutter_datadog');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<dynamic> init(String token, String environment) async {
    final Map<String, String> params = <String, String>{
      "token": token,
      "environment": environment
    };

    return await _channel.invokeMethod('initialize', params);
  }

  static Future<dynamic> log(LogLevel level, String event, Map<String, dynamic> attributes) async {
    final Map<String, dynamic> params = <String, dynamic>{
      "level": level.toString().replaceAll('LogLevel.', ''),
      "event": event
    };
    params.addAll(attributes);
    return await _channel.invokeMethod('log', params);
  }
}
