#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint datadog.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_datadog'
  s.version          = '0.0.1'
  s.summary          = 'Flutter plugin for sending logs to Datadog'
  s.description      = <<-DESC
Flutter plugin for sending logs to Datadog
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'DatadogSDK'
  s.ios.deployment_target = '11.0'
  s.swift_version = '5.0'
end
