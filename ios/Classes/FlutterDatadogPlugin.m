#import "FlutterDatadogPlugin.h"
#if __has_include(<flutter_datadog/flutter_datadog-Swift.h>)
#import <flutter_datadog/flutter_datadog-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_datadog-Swift.h"
#endif

@implementation FlutterDatadogPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterDatadogPlugin registerWithRegistrar:registrar];
}
@end
