import Flutter
import UIKit
import Datadog

public class SwiftFlutterDatadogPlugin: NSObject, FlutterPlugin {
    private static let METHOD_INIT = "initialize"
    private static let METHOD_LOG = "log"
    private var logger: Logger!
    private let registrar: FlutterPluginRegistrar

    init(registrar: FlutterPluginRegistrar) {
        self.registrar = registrar
    }

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_datadog", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterDatadogPlugin(registrar: registrar)
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch(call.method) {
          case SwiftFlutterDatadogPlugin.METHOD_INIT:
              let arguments = call.arguments as! [String: Any]
              let token = arguments["token"] as! String
              let environment = arguments["environment"] as! String

              Datadog.initialize(
                  appContext: .init(),
                  configuration: Datadog.Configuration
                      .builderUsing(clientToken: token, environment: environment)
                      .set(serviceName: "WP iOS")
                      .build()
              )

              logger = Logger.builder
                  .sendNetworkInfo(true)
                  .build()

              result(true)

          case SwiftFlutterDatadogPlugin.METHOD_LOG:
              let arguments = call.arguments as! [String: Any]
              let level = arguments["level"] as! String
              let event = arguments["event"] as! String

             let partyId = arguments["partyId"] as! String
             let profileId = arguments["profileId"] as! String
             let name = arguments["name"] as! String
             let sessionId = arguments["sessionId"] as! String
             let projectVersion = arguments["projectVersion"] as! String
             let txQuality = arguments["txQuality"] as! String
             let rxQuality = arguments["rxQuality"] as! String
             let localVideoStatsAgora = arguments["localVideoStatsAgora"] as! String
             let remoteVideoStatsAgora = arguments["remoteVideoStatsAgora"] as! String
             let localState = arguments["localState"] as! String
             let agoraError = arguments["agoraError"] as! String
             let message = arguments["message"] as! String

              if (self.logger != nil) {
                self.logger.addAttribute(forKey: "partyId", value: partyId);
                self.logger.addAttribute(forKey: "profileId", value: profileId);
                self.logger.addAttribute(forKey: "name", value: name);
                self.logger.addAttribute(forKey: "sessionId", value: sessionId);
                self.logger.addAttribute(forKey: "projectVersion", value: projectVersion);
                self.logger.addAttribute(forKey: "txQuality", value: txQuality);
                self.logger.addAttribute(forKey: "rxQuality", value: rxQuality);
                self.logger.addAttribute(forKey: "localVideoStatsAgora", value: localVideoStatsAgora);
                self.logger.addAttribute(forKey: "remoteVideoStatsAgora", value: remoteVideoStatsAgora);
                self.logger.addAttribute(forKey: "localState", value: localState);
                self.logger.addAttribute(forKey: "agoraError", value: agoraError);
                self.logger.addAttribute(forKey: "message", value: message);

                if (level == "WARN") {
                    self.logger.warn(event);
                } else if (level == "ERROR") {
                    self.logger.error(event);
                } else {
                    self.logger.info(event);
                }
              }

              result(true)

          default:
              result(false)
        }
    }
}
